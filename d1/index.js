console.log("Hello World");

//Arithmetic Operators

	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

//Assignment Operators

	//Basic Assignment operator
	
	let	assignmentNumber = 8;

	//Addition Assignment Operator
	//Addition Assignment Operator adds the value of the right operand to a variable and assigns the result to the variable.

	assignmentNumber = assignmentNumber + 2;
	console.log("Result of addition operator: " + assignmentNumber);
	assignmentNumber += 2;
	console.log("Result of addition assignment operator: " + assignmentNumber);

	//Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)

	assignmentNumber -= 2;
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	assignmentNumber *= 2;
	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	assignmentNumber /= 2;
	console.log("Result of division assignment operator: " + assignmentNumber);

//Multiple Operators and Parenthesis

	/*
		-When multiple operators are applied in a single statement, it follows the PEMDAS rule.
	*/

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operation: " + mdas); //0.6

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of pemdas operation: " + pemdas); //o.1999

	/*
		-By adding parenthesis "()" to create more complex computations will change the order of operations still ff the same rule".
	*/

//Increment and Decrement
	//Operators that add or subtract values by 1 and re-assigns the value of the variable where the increment/decrement was applied to

	let z = 1;
	let increment = ++z;
	console.log("Result of pre-increment: " + increment); //2
	console.log("Result of pre-increment: " + z); //2

	increment = z++;
	console.log("Result of post-increment: " + increment); //2
	console.log("Result of post-increment: " + z); //3

	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement); //2
	console.log("Result of pre-decrement: " + z); //2

	decrement = z--;
	console.log("Result of post-decrement: " + decrement); //2
	console.log("Result of post-decrement: " + z); //1

//Type Coercion

	let numA = '10';
	let numB = 12;

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	let numE = true + 1;
	console.log(numE); //2

	let numF = false + 1;
	console.log(numF);

	//Comparison Operators

	let juan = 'juan';

	//Equality Operator (==)

	console.log(1 == 1); //true
	console.log(1 == 2); //false
	console.log(1 == '1'); //true because it converts even diff data type
	console.log(0 == false) //true since in boolean we associate false to 0
	console.log('juan' == 'juan'); //true
	console.log('juan' == juan); //true because we declare var juan is juan

	//Inequality Operator

	console.log(1 != 1); //false
	console.log(1 != 2); //true
	console.log(1 != '1'); //false
	console.log(0 != false) //false 
	console.log('juan' != 'juan'); //false
	console.log('juan' != juan); //false

	//Strict Equality Operator

	console.log(1 === 1); //true
	console.log(1 === 2); //false
	console.log(1 === '1'); //false
	console.log(0 === false) //false 
	console.log('juan' === 'juan'); //true
	console.log('juan' === juan); //true

	//Strict Inequality Operator

	console.log(1 !== 1); //false
	console.log(1 !== 2); //true
	console.log(1 !== '1'); //true
	console.log(0 !== false) //true
	console.log('juan' !== 'juan'); //false
	console.log('juan' !== juan); //false

	//Relational Operators

	let a = 50;
	let b = 65;

	let isGreaterThan = a > b;
	let isLessThan = a < b;
	let isGTorEqual = a >= b;
	let isLTorEqual = a <= b;

	console.log(isGreaterThan); //false
	console.log(isLessThan); //true
	console.log(isGTorEqual); //false
	console.log(isLTorEqual); //true

	let numStr = '30';
	//coercion to change the Str to a Num
	console.log(a > numStr); //true
	console.log(b <= numStr); //false

	let str = "twenty";
	console.log(b >= str); //false since string is not a number, the string was converted to a number

	//Logical Operator (AND - &&) T if both operand is T

	let isLegalAge = true;
	let isRegistered = false;

	let allRequirementsMet = isLegalAge && isRegistered;
	console.log('Result of Logical AND Operator: ' + allRequirementsMet); //false

	//Logical Operator (OR - ||) T if one operand is T

	let someRequirementsMet = isLegalAge || isRegistered;
	console.log('Result of Logical OR Operator: ' + someRequirementsMet); //true

	//Logical Operator (NOT - !) returns the opposite value

	let someRequirementsNotMet = !isRegistered;
	console.log('Result of Logical NOT Operator: ' + someRequirementsNotMet); //true

	//typeof operator is used to check the datat type of a value or expressiona nd it returns a  string value of what the data type is.  
